class GreenLine
  def initialize(date)
    @date = date.to_date
  end

  def data
    Nordpool::FourRangeData.new(@date).prices.map do |d|
      {id: d[:id], limit: 1000 / d[:value]}
    end.sort_by{|d| d[:id]}
  end
end
