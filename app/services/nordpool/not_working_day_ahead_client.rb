# frozen_string_literal: true
require 'net/http'
require 'net/https'

module Nordpool
  class NotWorkingDayAheadClient
    def initialize
    end

    def token
      get_token
    end

    private

    def http
      @http ||= begin
        uri = URI('https://sts.nordpoolgroup.com')
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http
      end
    end

    def get_token
      request = Net::HTTP::Post.new('/connect/token')
      request.set_form_data({
        grant_type: 'password',
        scope: 'dayahead_api',
        username: 'nordpool2016',
        password: 'nordpool2016',
      })
      p authorization
      request['Authorization'] = authorization
      http.request(request)
    end

    def authorization
      @authorization ||= begin
        'Basic ' + Base64.strict_encode64(client_string+':'+client_string)
      end
    end

    def client_string
      'client_dayahead_api'
    end
  end
end
