module Nordpool
  class FourRangeData < Struct.new(:date)
    def prices
      day = date.to_date
      Rails.cache.fetch(['four_range_data', day]) do
        result = Hash.new(0)

        if day.past?
          past_data
        else
          client = HourPriceClient.new
          client.prices(day)
        end.each do |row|
          segment = row[:start_time].hour / 3
          result[segment] += row[:value]
        end
        result.map{|k, v| {id: k, value: v / 3.0 / 1000.0}}
      end
    end

    private

    def past_data
      HourData.where(hour: Time.parse('2016-01-21').all_day).map do |d|
        {start_time: d.hour, value: d.price}
      end
    end
  end
end
