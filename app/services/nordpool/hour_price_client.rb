# frozen_string_literal: true
require 'net/http'
require 'net/https'

module Nordpool
  class HourPriceClient
    HOURLY = '10'

    def initialize
    end

    def prices(date)
      date = date.to_date
      data = fetch(date)
      parse_data_per_hour(data)
    end

    private

    def areas
      @areas ||= ['FI']
    end

    def http
      @http ||= begin
        uri = URI('https://www.nordpoolspot.com')
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http
      end
    end

    def fetch(date)
      request = Net::HTTP::Get.new('/api/marketdata/page/' + HOURLY)
      puts date
      request.set_form_data({
        endDate: date.strftime('%Y-%m-%d'),
        areas: areas.join,
        currency: 'EUR',
      })
      request['Content-Type'] = 'application/json'
      # TODO: process errors
      JSON.parse(http.request(request).body, symbolize_names: true)
    end

    # value per megabyte
    def parse_data_per_hour(data)
      data = data[:data]
      result = []

      data[:Rows].each do |row|
        row[:Columns].each do |column|
          name = column[:Name]
          next if areas.exclude?(name)
          next if row[:IsExtraRow]
          # price for kilowatts per hour
          result << {start_time: Time.parse(row[:StartTime]), value: column[:Value].to_f}
        end
      end
      result
    end
  end
end
