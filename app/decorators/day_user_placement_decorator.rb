class DayUserPlacementDecorator < Draper::Decorator
  delegate_all

  def status
    case rand(3)
    when 0
      :none
    when 1
      :good
    when 2
      :bad
    end
  end
end
