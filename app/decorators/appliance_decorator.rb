class ApplianceDecorator < Draper::Decorator
  delegate_all

  def consumption
    object.consumption / 1000.0
  end
end
