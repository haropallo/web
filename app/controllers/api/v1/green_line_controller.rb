module Api
  module V1
    class GreenLineController < Api::ApplicationController
      def index
        render json: GreenLine.new(params[:date]).data
      end
    end
  end
end
