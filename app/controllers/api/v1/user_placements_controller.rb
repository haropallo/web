module Api
  module V1
    class UserPlacementsController < Api::ApplicationController
      before_action :load_user
      before_action :load_placements, only: :index

      def index
        render json: @placements
      end

      def create
        params = create_params.merge(user: @user)
        DayUserPlacement.create(params)
      end

      private

      def load_user
        @user = User.first
      end

      def load_placements
        unless @user
          render json: []
        else
          @placements = @user.placements.decorate
        end
      end

      def create_params
        params.permit(:day, placement: [:id, :time_segment_id])
      end
    end
  end
end
