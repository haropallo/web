module Api
  module V1
    class PricesController < Api::ApplicationController
      def index
        render json: Nordpool::FourRangeData.new(params[:date]).prices
      end
    end
  end
end
