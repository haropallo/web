module Api
  module V1
    class AppliancesController < Api::ApplicationController
      def index
        @appliances = Appliance.all.decorate
        render json: @appliances.to_json
      end
    end
  end
end
