module Api
  module V1
    class BaseConsumptionsController < Api::ApplicationController
      def index
        render json: [
          {name: 'Building 1', consumptions: consumption(1)},
          {name: 'Building 11', consumptions: consumption(11)},
          {name: 'Building 17', consumptions: consumption(17)},
        ]
      end

      private

      def consumption(building)
        Rails.cache.fetch([:base_consumptions, building]) do
          Net::HTTP.get(URI("https://gitlab.com/haropallo/background_data/raw/master/for%20app/powerTodayBuilding#{building}.csv")).split.map{|value| value.to_f / 1000.0}
        end
      end
    end
  end
end
