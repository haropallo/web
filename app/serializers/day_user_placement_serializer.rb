class DayUserPlacementSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :day, :placement, :status
end
