class CreateDayUserPlacements < ActiveRecord::Migration[5.0]
  def change
    create_table :day_user_placements do |t|
      t.references :user
      t.date :day
      t.jsonb :placement
      t.timestamps
    end
  end
end
