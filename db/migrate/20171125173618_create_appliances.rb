class CreateAppliances < ActiveRecord::Migration[5.0]
  def change
    create_table :appliances do |t|
      t.string :name
      t.timestamps
    end
  end
end
