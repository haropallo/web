class CreateHourPowerLimits < ActiveRecord::Migration[5.0]
  def change
    create_table :hour_datas do |t|
      t.timestamp :hour
      t.decimal :power_consumption_limit, precision: 12, scale: 7
      t.decimal :price, precision: 12, scale: 7
      t.decimal :renewable_production, precision: 15, scale: 7
      t.decimal :co2_coeff, precision: 12, scale: 10
      t.decimal :total_production, precision: 15, scale: 7
      t.timestamps
    end
  end
end
