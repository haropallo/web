class AddConsumptionToAppliances < ActiveRecord::Migration[5.0]
  def change
    add_column :appliances, :consumption, :decimal, precision: 7, scale: 2
  end
end
