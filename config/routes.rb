Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  namespace :api, default: { format: :json } do
    namespace :v1 do
      resources :prices, only: :index
      resources :appliances, only: :index
      resources :green_line, only: :index
      resources :user_placements, only: [:index, :create]
      resources :base_consumptions, only: [:index]
    end
  end
end
