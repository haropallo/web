# frozen_string_literal: true
task add_prices: [:environment] do
  # Why the fuck xls contains html in cells?
  doc = Roo::Spreadsheet.open('vendor/elspot-prices_2017_hourly_eur.xlsx')
  doc.default_sheet = doc.sheets.last

  read_row = -> (r) { doc.cell(r,'F') }

  r = 16
  while row = read_row.call(r)
    tds = Nokogiri::HTML(row).xpath('//td')
    date, hour = tds.shift.content.to_date, tds.shift.content.split('-').first.to_i
    sys, se1, se2, se3, se4, fi, dk1, dk2, oslo, kr_sand, bergen, molde, tr_heim, tromso, ee, lv, lt = tds.map{|v| v.content.tr(',', '.').to_d }

    hour = date.to_time + hour.hours
    hour_data = HourData.find_or_create_by(hour: hour)
    hour_data.update(price: fi)
    puts "#{hour}: #{fi}"
    r += 2
  end
end

task add_bad_day: :environment do
  # doc = Roo::Spreadsheet.open('vendor/21_01_16.xlsx')
  # doc.default_sheet = doc.sheets.last

  # HourData.where(hour: Time.parse('2016-01-16').all_day).delete_all

  # doc.each(start_time: 'start_time', total: 'total', hydro: 'hydro', wind: 'wind', nuclear: 'nuclear') do |row|
  #   HourData.

  #   puts row
  # end


  co2_coeff1 = 0.2225457295
  co2_coeff2 = 0.187118

  date = Date.parse('2016-01-21')
  %w[29,65
    27,82
    26,65
    28,05
    35,26
    56,01
    95,03
    199,96
    214,25
    200
    130,06
    74,97
    61,39
    59,11
    60,01
    98,6
    199,97
    200,09
    199,87
    61,61
    50,09
    37,28
    32,21
    28,07].map.with_index do |price, hour_index|
    puts hour_index, price.tr(',', '.').to_f
    hour_data = HourData.find_or_create_by(hour: date.to_time + hour_index.hours)
    hour_data.update(price: price.tr(',', '.').to_f)
  end
end
