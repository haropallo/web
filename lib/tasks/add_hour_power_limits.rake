task add_hour_power_limits: :environment do
  ((Date.today - 20)..Date.today).each do |day|
    24.times do |hour_index|
      hour = day.to_time + hour_index.hours
      HourPowerLimit.create(hour: hour, value: rand*100)
    end
  end
end
