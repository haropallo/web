task add_appliances: :environment do
  Appliance.destroy_all
  Appliance.create([
    {id: 0, name: 'Dryer', consumption: 2000},
    {id: 1, name: 'Washing machine', consumption: 700},
    {id: 2, name: 'Dish washer', consumption: 533.3},
    {id: 3, name: 'Oven', consumption: 1333.3},
    {id: 5, name: 'Sauna', consumption: 2666.7},
    {id: 6, name: 'Steam iron', consumption: 500},
    {id: 7, name: 'Vacuum cleaner', consumption: 500},
    {id: 8, name: 'Cooker', consumption: 266.7},
  ])
end
